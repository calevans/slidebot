#! /bin/bash
cd ~ && touch ~/.bash_profile
cd /tmp && ([[ -d sexy-bash-prompt ]] || git clone --depth 1 --config
  core.autocrlf=false https://github.com/twolfson/sexy-bash-prompt) && cd
  sexy-bash-prompt && make install
