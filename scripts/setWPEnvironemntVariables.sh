#! /bin/bash
echo "Building .env file"
echo "# This file is automatically regenerated each time lando is restarted" > /app/.env
echo " " >> /app/.env

echo DB_NAME="'"$(echo $LANDO_INFO | jq -r .database.creds.database)"'" >> /app/.env
echo DB_USER="'"$(echo $LANDO_INFO | jq -r .database.creds.user)"'" >> /app/.env
echo DB_PASSWORD="'"$(echo $LANDO_INFO | jq -r .database.creds.password)"'" >> /app/.env
echo DB_NAME="'"$(echo $LANDO_INFO | jq -r .database.creds.database)"'" >> /app/.env
echo DB_HOST="'"$(echo $LANDO_INFO | jq -r .database.internal_connection.host)"'" >> /app/.env
echo WP_HOME="'"$(echo $LANDO_INFO | jq -r  .appserver.urls[1])"'" >> /app/.env
echo WP_SITEURL="'"$(echo $LANDO_INFO | jq -r  .appserver.urls[1])/wp"'" >> /app/.env

# Yes, I know it's bad to hard code these.
echo DB_PREFIX="'"wp_"'" >> /app/.env
echo WP_ENV="'"staging"'" >> /app/.env



