#!/bin/bash
echo "BEGIN FIXING SSH"
#
# Out with the old
#
echo " " > /etc/ssh/ssh_config

#
# In with the new
#
cp /lando_keys/* ~/.ssh
chmod 600 ~/.ssh/*

# Test to make sure it works
if [[ $(ssh -T git@gitlab.com 2>&1) == "Welcome to GitLab"* ]]; then
        echo "SSH WORKING!"
else
        echo "SSH NOT WORKING!"
fi

echo "END FIXING SSH"
