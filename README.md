# Slidebot

(c) EICC, Inc.

It's a bot...for your slides.

Installation

- Install [Lando](https://lando.dev/)
- Clone this repo
- In the slidebot dir run `lando start`
- Go to /app/web/ and run `git clone git@gitlab.com:calevans/slidebot-api.git api`
- Follow instructuons in the API readme dir.
- Go to the URL presented and finish the WP install
- Log into WordPress
- Create a presentation
- More to come